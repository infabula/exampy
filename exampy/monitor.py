class MonitorBrokeException(Exception):
    pass


class Monitor:
    def __init__(self, brand, resolution):
        self.brand = brand
        self.set_resolution(resolution)
        self.is_on = False
        self.color_temp = '65K'

    def turn_on(self):
        self.is_on = True


    def set_resolution(self, resolution):
        if resolution[0] < 0:
            raise ValueError("X dimension should be positive")
        if resolution[1] < 0:
            raise ValueError("Y dimension should be positive")
        self._resolution = resolution


    def get_resolution(self):
        return self._resolution


if __name__ == "__main__":
    mon = Monitor(brand='HP',
                  resolution=(1920, 1080))
    print(mon.get_resolution())
    print(mon.is_on)
    mon.turn_on()
    print(mon.is_on)

